	.text
	.global 	_start

_start:	movia	r2, 0x10000060					# using Expansion Parallel Port JP1
		movia 	r3, CONTROLLER_BITS
		movia	sp, 8000
		
		addi	r20, r0, 1						# TOLERANCE to tilting, if the difference is less than specified, don't bother tilting.
		movia 	r16, 30000						# ON DURATION
		movia	r17, 30000						# OFF DURATION
		
		ldw		r4, 4(r3)						# store data value for Lega GPIO
		stwio	r4, (r2)						# write into JP1 data direction
		
		ldw		r4, 0(r3)						# store data direction for Lega GPIO
		stwio	r4, 4(r2)						# write into JP1 data direction
		stw 	r4, 0(r3)						# store back into memory too	
		
		
		# set to polling state, write 1 to GP21
		movi	r7, 21
		call 	WRITE_1_TO_GPIO
		
LOOP:	
		# disable the other sensors by writing 1 to it
		movi	r7, 12
		call	WRITE_1_TO_GPIO
		movi	r7, 14
		call	WRITE_1_TO_GPIO
		movi	r7, 16
		call	WRITE_1_TO_GPIO
		movi	r7, 18
		call	WRITE_1_TO_GPIO
		# read from sensor 0, write 0 to GP10
		movi	r7, 10
		call	WRITE_0_TO_GPIO
		
		CHECK_SENSOR0_VALID:			
			ldwio	r5, 0(r2)					# read from JP1
			addi	r7, r0, 1					# attempt to mask other bits other than bit 11
			slli	r7, r7, 11					# shift to read GP11
			and		r5, r5, r7
			bne 	r5, r0, CHECK_SENSOR0_VALID	# if sensor 0 data is invalid (1), keep checking
			
		# read from sensors from GP27, 30
		ldwio	r5, 0(r2)						# read from JP1
		srli	r5, r5, 27						# shift right by 27 to move 27-30 to 0-3 bit
		addi	r8, r0, 1
		slli	r8, r8, 4						# shift by 4 to mask the remaining bit 31 that wasn't masked by previous shifting
		nor		r8, r8, r8
		and		r5, r5, r8
		mov 	r22, r5							# STORE TO REGISTER 22 FOR COMPARISON
					
		# disable the other sensors by writing 1 to it
		movi	r7, 10
		call	WRITE_1_TO_GPIO
		movi	r7, 14
		call	WRITE_1_TO_GPIO
		movi	r7, 16
		call	WRITE_1_TO_GPIO
		movi	r7, 18
		call	WRITE_1_TO_GPIO
		# read from sensor 1, write 0 to GP12
		movi	r7, 12
		call	WRITE_0_TO_GPIO
		
		CHECK_SENSOR1_VALID:
			ldwio	r5, 0(r2)					# read from JP1
			addi	r7, r0, 1					# attempt to mask other bits other than bit 11
			slli	r7, r7, 13					# shift to read GP13
			and		r5, r5, r7
			bne 	r5, r0, CHECK_SENSOR1_VALID	# if sensor 0 data is invalid (1), keep checking
		
		# read from sensors from GP27, 30
		ldwio	r5, 0(r2)						# read from JP1
		srli	r5, r5, 27						# shift right by 27 to move 27-30 to 0-3 bit
		addi	r8, r0, 1
		slli	r8, r8, 4						# shift by 4 to mask the remaining bit 31 that wasn't masked by previous shifting
		nor		r8, r8, r8
		and		r5, r5, r8
		mov 	r21, r5							# STORE TO REGISTER 21 FOR COMPARISON
		
		bgtu	r22, r21, TURN_COUNTERCLOCKWISE	# if r22 (sensor 0) > r21 (sensor 1) - which means the distance between sensor 0 and ground is larger, so we need to rotate towards r22, which is counterclockwise
		
		TURN_CLOCKWISE:
		sub		r19, r21, r22
		bgtu	r20, r19, CONTINUE				# if r20 is less than the TOLERANCE r19, then don't do anything
		
		movi	r7, 1
		call	WRITE_0_TO_GPIO					# tell Lego to turn clockwise
		mov		r7, r0
		
		call 	WRITE_0_TO_GPIO					# enable the motor		
		mov	 	r10, r16
		call	DELAY_TIMER
		call 	WRITE_1_TO_GPIO					# disable the motor
		mov		r10, r17
		call	DELAY_TIMER
		
		br 		LOOP
		
		TURN_COUNTERCLOCKWISE:
		sub		r19, r22, r21
		bgtu	r20, r19, CONTINUE				# if r20 is less than the TOLERANCE r19, then don't do anything
		
		movi	r7, 1
		call	WRITE_1_TO_GPIO					# tell Lego to turn counter clockwise
		mov		r7, r0
		
		call 	WRITE_0_TO_GPIO					# enable the motor
		mov	 	r10, r16
		call	DELAY_TIMER
		call 	WRITE_1_TO_GPIO					# disable the motor
		mov		r10, r17
		call	DELAY_TIMER
		
		br 		LOOP
		
		CONTINUE:		
			mov		r7, r0
			call 	WRITE_1_TO_GPIO				# disable the motor
		
		br LOOP
		
WRITE_1_TO_GPIO:								# r7 as input, no output
	subi	sp, sp, 12
	stw		r5, 0(sp)
	stw		r6, 4(sp)
	stw 	ra, 8(sp)
	
	ldwio	r5, 0(r2)							# read current state of JP1
	addi	r6, r0, 1
	sll		r6, r6, r7							# shift by an amount of bit stated in r7
	or		r5, r5, r6
	stwio	r5, 0(r2)							# write into JP1
	
	ldw		r5, 0(sp)
	ldw		r6, 4(sp)
	ldw 	ra, 8(sp)
	addi	sp, sp, 12
	ret
	
WRITE_0_TO_GPIO:								# r7 as input, no output
	subi	sp, sp, 12
	stw		r5, 0(sp)
	stw		r6, 4(sp)
	stw 	ra, 8(sp)
	
	ldwio	r5, 0(r2)							# read current state of JP1
	addi	r6, r0, 1
	sll		r6, r6, r7							# shift by an amount of bit stated in r7
	nor		r6, r6, r6							# invert all bits
	and		r5, r5, r6							# use and to write a 0 while not affecting the other bits
	stwio	r5, 0(r2)							# write into JP1
	
	ldw		r5, 0(sp)
	ldw		r6, 4(sp)
	ldw 	ra, 8(sp)
	addi	sp, sp, 12
	ret
	
DELAY_TIMER:									# input as r10
	subi	sp, sp, 16
	stw		r18, 0(sp)
	stw 	r12, 4(sp)
	stw		r19, 8(sp)
	stw 	r13, 12(sp)	
	
	movia 	r18, 0x10002000						# status register
	mov 	r12, r10
	sthio 	r12, 8(r18)							# store half word in low counter 
	srli 	r12, r12, 16						# lofical shift right by 16 
	sthio 	r12, 12(r18)						# store half word in high counter 
	movi 	r19, 0b0110							# control register: set START=1 and CONT=1
	sthio 	r19, 4(r18)							# write into control register 
	
	DELAY:
		ldwio r13, (r18)						# read status register
		andi r13, r13, 1						# mask all other bit except the TO bit at lst LSB position
		beq r13, r0, DELAY						# if it's not 1 yet, keep polling
	stwio r13, (r18)							# if it's 1 already, write r7 into status register to clear TO bit
	movi 	r19, 0b1000							# control register: set STOP=1
	sthio 	r19, 4(r18)							# write into control register 
	
	ldw		r18, 0(sp)
	ldw 	r12, 4(sp)
	ldw		r19, 8(sp)
	ldw 	r13, 12(sp)
	addi	sp, sp, 16
	
	ret
	
	.data
	CONTROLLER_BITS:
	.word	0b00000000011000000001011111111111	# GPIO for Lego controller Data Direction, 1 as output, 0 as input
	.word	0b11111111111111111111111111111111	# disable all GPIOs
	.end